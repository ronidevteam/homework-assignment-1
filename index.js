// # Homework Assignment 1 - Simple "Hello World" API
//
// Requirements:
// - It should be a RESTful JSON API that listens on a port of your choice.
// - When someone posts anything to the route /hello, you should return a welcome message, in JSON format.

// Dependencies
const http = require('http');
const url = require('url');

// Create a http server
const server = http.createServer((req, res) => {
  // Get the URL from the request and parse it
  const parsedUrl = url.parse(req.url, true);

  // Get the path
  const path = parsedUrl.pathname;
  const trimmedPath = path.replace(/^\/+|\/+$/g, '');

  req.on('data', data => {});

  req.on('end', data => {
    // Choose the handles this request should go to. If one not found use the notFound handler
    const choosenHandler =
      typeof router[trimmedPath] !== 'undefined'
        ? router[trimmedPath]
        : handlers.notFound;

    // Router the request to the handler specified in the router
    choosenHandler(data, (statusCode, payload) => {
      // Use the status code passed by the handler or default to 200
      statusCode = typeof statusCode == 'number' ? statusCode : 200;

      // Use the payload passed by the handler, or default to an empty object
      payload = typeof payload == 'object' ? payload : {};

      // Conver the payload to a string
      const payloadString = JSON.stringify(payload);

      // Return the response
      res.setHeader('Content-Type', 'application/json');
      res.writeHead(statusCode);
      res.end(payloadString);
      // Log the response status code and payload
      console.log('Returning this response: ', statusCode, payloadString);
    });
  });
});

// Start listening on port 3000
server.listen(3000, () => {
  console.log('Listening on port 3000');
});

const handlers = {
  hello(data, callback) {
    // Callback a http status code, and a payload object
    callback(200, {
      message: 'Hi, there! Thank you for making a request to my API!'
    });
  },
  notFound(data, callback) {
    callback(404);
  }
};

// Defining a request router
const router = {
  hello: handlers.hello
};
