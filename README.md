# Homework Assignment 1

Simple "Hello World" API. 

## Requirements:
- It should be a RESTful JSON API that listens on any port. 
- When someone posts anything to the route /hello, it should return a welcome message, in JSON format.

## Setup:
- Clone the repo
- Navigate to the repo from the terminal
- Once you are inside the folder, run ``` node index.js ```. This will start a server that listens for requests on port 3000

## Using the API:
- Make a POST request to localhost:3000/hello. This should return a response with status code 200 and a message saying 'Hi there, thank you for using my API'